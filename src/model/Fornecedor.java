package model;

import java.util.ArrayList;



/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alunos
 */
public class Fornecedor {
    private String nome;
    private ArrayList<String> telefones;
    private Endereco endereco;
    private ArrayList<Produto> produtos;

    public Fornecedor(String nome, String telefone) {
        this.nome = nome;
        telefones.add(telefone);
    }

    public Fornecedor() {
    }

    public void setProdutos(ArrayList<Produto> produtos) {
        this.produtos = produtos;
    }

    public void setTelefones(ArrayList<String> telefones) {
        this.telefones = telefones;
    }

    public ArrayList<String> getTelefones() {
        return telefones;
    }

    public void setEndereco(Endereco endereco) {
        this.endereco = endereco;
    }

    public ArrayList<Produto> getProdutos() {
        return produtos;
    }


    public void setNome(String nome) {
        this.nome = nome;
    }


    public Endereco getEndereco() {
        return endereco;
    }

    public String getNome() {
        return nome;
    }
    
}
