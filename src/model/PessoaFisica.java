package model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alunos
 */
public class PessoaFisica extends Fornecedor {
    private String cpf;

    public PessoaFisica(String nome, String telefone, String cpf){
        super(nome, telefone);
        this.cpf = cpf;
    }
    
    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getCpf() {
        return cpf;
    }
}
