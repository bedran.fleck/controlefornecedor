package model;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author alunos
 */
public class Produto {
    private String Nome;
    private String descricao;
    private String valorCompra;
    private String valorVenda;
    private String quantidadeEstoque;

    public void setNome(String Nome) {
        this.Nome = Nome;
    }

    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    public void setQuantidadeEstoque(String quantidadeEstoque) {
        this.quantidadeEstoque = quantidadeEstoque;
    }

    public void setValorCompra(String valorCompra) {
        this.valorCompra = valorCompra;
    }

    public void setValorVenda(String valorVenda) {
        this.valorVenda = valorVenda;
    }

    public String getNome() {
        return Nome;
    }

    public String getDescricao() {
        return descricao;
    }

    public String getQuantidadeEstoque() {
        return quantidadeEstoque;
    }

    public String getValorCompra() {
        return valorCompra;
    }

    public String getValorVenda() {
        return valorVenda;
    }
}
