/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.ArrayList;
import model.*;

/**
 *
 * @author alunos
 */
public class controleFornecedor {

    private ArrayList<Fornecedor> listaFornecedores;
    
    Fornecedor umFornecedor = new Fornecedor();

    public controleFornecedor(ArrayList<Fornecedor> listaFornecedores) {
        this.listaFornecedores = listaFornecedores;
    }
    
    
    public String adicionar(PessoaFisica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa física adicionado com sucesso!";
    }
    
    public String adicionar(PessoaJuridica fornecedor){
        listaFornecedores.add(fornecedor);
        return "Fornecedor Pessoa jurídica adicionado com sucesso!";
    }
    
    public String remover(PessoaFisica fornecedor){
        listaFornecedores.remove(fornecedor);
        return "Fornecedor removido com sucesso!";
    }
    
    public String remover(PessoaJuridica fornecedor){
        listaFornecedores.remove(fornecedor);
        return "Fornecedor removido com sucesso!";
    }
    
    public Fornecedor pesquisarNome(String nome){
        for(Fornecedor umFornecedor : listaFornecedores)
            if(umFornecedor.getNome().equalsIgnoreCase(nome)){
                return umFornecedor;
            }
        return null;
    }
    
    public String clearAll(){
        listaFornecedores.clear();
        return "A lista de fornecedores foi esvaziada.";
    }

}
